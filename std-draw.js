function stdDraw() {
};
/**
 * Initialize stdDraw object to work with specific canvas
 * @param id the id of canvas element on html
 * @param canvasSizeW the width of canvas html element
 * @param canvasSizeH the height of canvas html element
 */
stdDraw.prototype.init = function (id, canvasSizeW, canvasSizeH) {
    this.minX = 0;
    this.maxX = 1;
    this.minY = 0;
    this.maxY = 1;

    // get canvas by id
    this.canvas = document.getElementById(id);

    // save size in variables
    this.defaultCanvasSizeW = canvasSizeW;
    this.defaultCanvasSizeH = canvasSizeH;

    //setup canvas
    this.canvas.width = this.defaultCanvasSizeW;
    this.canvas.height = this.defaultCanvasSizeH;

    // get context
    this.graphics = this.canvas.getContext("2d");

    // calculate ratio
    this.ratio = canvasSizeW / canvasSizeH;
};

/**
 * scale (magnify) image for output in canvas
 * @param scaleX magnify x by scaleX
 * @param scaleY magnify y by scaleY
 */
stdDraw.prototype.applyScale = function (scaleX, scaleY) {
    this.scaleX = scaleX - 0;
    this.scaleY = scaleY - 0;
    this.graphics.scale(this.defaultCanvasSizeW, this.defaultCanvasSizeH);
    this.graphics.lineWidth = 1 / this.defaultCanvasSizeW;
};

/**
 * move center to new position.
 * @param x the x should be greater 0 and less 1
 * @param y the y should be greater 0 and less 1
 */
stdDraw.prototype.setCenter = function (x, y) {
    this.minX = this.minX - x;
    this.maxX = this.minX + x + x;
    this.minY = this.minY - y;
    this.maxY = this.minY + y + y;
    this.graphics.translate(x, y);
};

/**
 * clear canvas
 */
stdDraw.prototype.clear = function () {
    this.graphics.clearRect(this.minX, this.minY, (this.maxX - this.minX) * this.ratio, this.maxY - this.minY);
};

/**
 * Draw a filled square of side length 2r, centered on (x, y).
 * @param x the x-coordinate of the center of the square
 * @param y the y-coordinate of the center of the square
 * @param r radius is half the length of any side of the square
 * @throws IllegalArgumentException if r is negative
 */
stdDraw.prototype.filledSquare = function (x, y, r, color) {
    this.graphics.fillStyle = color;

    var ws = this.factorXScreen() * r;
    var hs = this.factorYScreen() * r;
    var xs = this.factorXScreen() * x - this.factorXScreen() / 2 - ws;
    var ys = this.factorYScreen() * y - this.factorYScreen() / 2 - hs;

    this.graphics.fillRect(xs, ys, ws * 2, hs * 2);
};

/**
 * draw a line from point (x1, y1) to point (x2, y2) width width lw
 * @param x1 the first point x coordinate
 * @param y1 the first point y coordinate
 * @param x2 the second point x coordinate
 * @param y2 the second point y coordinate
 * @param lw the width of line
 */
stdDraw.prototype.line = function (x1, y1, x2, y2, lw) {
    if (!lw) {
        lw = 1;
    }
    this.graphics.beginPath();
    this.graphics.moveTo(x1 * this.factorXScreen(), y1 * this.factorYScreen());
    this.graphics.lineWidth = lw * this.factorXScreen();
    this.graphics.lineTo(x2 * this.factorXScreen(), y2 * this.factorYScreen());
    this.graphics.stroke();
};

/**
 * setup drawing style
 * @param style - color of stroke
 */
stdDraw.prototype.setStrokeStyle = function (style) {
    this.graphics.strokeStyle = style;
};

/**
 * get x coordinate scale factor
 * @returns {number}
 */
stdDraw.prototype.factorXScreen = function () {
    return 1 / this.scaleX;
};

/**
 * get y coordinate scale factor
 * @returns {number}
 */
stdDraw.prototype.factorYScreen = function () {
    return 1 / this.scaleY;
};
